require('./bootstrap');
require('./components');

const store = require('./store').default;
const routes = require('./routes').default;
const router = new VueRouter({ routes });

window.hub = new Vue();

const app = new Vue({ router, store }).$mount('#app');
