const VuexLocal = new VuexPersistence({
    storage: window.localStorage
});

export default new Vuex.Store({
    modules: {
        'character': require('./stores/Character').default,
        'inventory': require('./stores/Inventory').default,
        'items': require('./stores/Items').default
    },

    plugins: [
        VuexLocal.plugin
    ]
});
