Math.clamp = function(value, min, max)
{
    if (min && value < min) return min;
    if (max && value > max) return max;
    return value;
};
