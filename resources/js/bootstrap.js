import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VuexPersistence from 'vuex-persist';

require('./abstracts/Math');

window._ = require('lodash');

window.jQuery = require('jquery');

window.Vue = Vue;
window.VueRouter = VueRouter;
window.Vuex = Vuex;
window.VuexPersistence = VuexPersistence;

Vue.use(VueRouter);
Vue.use(Vuex);
