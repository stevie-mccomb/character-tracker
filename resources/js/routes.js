export default [
    {
        path: '/',
        component: require('./views/Character.vue').default
    },

    {
        path: '/inventory',
        component: require('./views/Inventory.vue').default
    }
];
