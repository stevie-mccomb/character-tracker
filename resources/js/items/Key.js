export default {
    id: 'key',

    name: 'Key',

    description: 'Just a plain old rusty key.',

    icon: 'fa-key'
};
