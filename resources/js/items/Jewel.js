export default {
    id: 'jewel',

    name: 'Jewel',

    description: 'An expensive, shiny gem.',

    icon: 'fa-diamond'
};
