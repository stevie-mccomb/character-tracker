export default {
    namespaced: true,

    state: {
        items: require('../items').default
    },

    getters: {
        item: (state) => (id) => {
            for (let item of state.items) {
                if (item.id === id) return item;
            }

            return null;
        }
    }
};
