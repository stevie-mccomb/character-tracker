export default {
    namespaced: true,

    state: {
        name: '',

        age: 0,

        bio: '',

        health: {
            min: 0,
            max: 100,
            current: 100
        },

        magic: {
            min: 0,
            max: 25,
            current: 25
        },

        stamina: {
            min: 0,
            max: 50,
            current: 50
        }
    },

    mutations: {
        name(state, value) {
            state.name = value;
        },

        age(state, value) {
            state.age = Math.clamp(parseInt(value), 0);
        },

        bio(state, value) {
            state.bio = value;
        },

        health(state, value) {
            state.health.current = Math.clamp(value, state.health.min, state.health.max);
        },

        magic(state, value) {
            state.magic.current = Math.clamp(value, state.magic.min, state.magic.max);
        },

        stamina(state, value) {
            state.stamina.current = Math.clamp(value, state.stamina.min, state.stamina.max);
        }
    }
};
