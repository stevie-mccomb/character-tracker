export default {
    namespaced: true,

    state: {
        items: []
    },

    mutations: {
        pickup(state, id) {
            let item = this.getters['items/item'](id);

            if (!item) return console.error(`No item found with ID of ${id}, could not pick up.`);

            state.items.push(id);
        },

        drop(state, id) {
            console.log(`Dropping: ${id}`);
            let index = state.items.indexOf(id);

            if (index >= 0) state.items.splice(index, 1);
        }
    }
};
