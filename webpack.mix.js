const mix = require('laravel-mix');

mix.options({
    processCssUrls: false,
    publicPath: 'public'
});

mix.js('resources/js/app.js', 'public/js');
mix.sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');

    mix.version();
}
